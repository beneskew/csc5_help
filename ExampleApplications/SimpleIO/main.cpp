/* 
 * File:        main.cpp
 * Author:      Ben Eskew
 * Definition:  This is an example command-line application which utilizes the IO Stream for gathering simple input from
 *              ...the user, doing a simple calculation with that input, then displaying the result back to the user.
 * 
 * Created on February 19, 2014, 12:07 AM
 * 
 */

#include <cstdlib>   // include the C Standard General Utilities Library
#include <iostream>  // include the C I/O Stream Library

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int i;                                              // declare an integer named i
    cout << "Please enter an integer value: ";          // display some text
    cin >> i;                                           // require an input value from the user, and place it in the integer i
    cout << "The value you entered is: " << i;          // display some text and the value which the user entered
    cout << " and its double is: " << i*2 << endl;      // display some text, calculate the value times 2, then display the result
    
    return 0;                                           // always return a value.
}