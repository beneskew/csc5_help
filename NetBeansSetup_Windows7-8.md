# NetBeans IDE 7.4 - Setup for Windows 7 and 8

This is a rough guide for installing and setting up NetBeans IDE 7.4 (for use as a C/C++ development environment) for Windows 7 and Windows 8. This is not meant to be a thourough guide. If you are having troubles with the installation or setup which are not outlined in this document, please refer to the NetBeans website at the following URLs:

- NetBeans IDE 7.4 General Information (https://netbeans.org/community/releases/74/index.html)
- NetBeans IDE 7.4 Installation Instructions (https://netbeans.org/community/releases/74/install.html)
- NetBeans Forums (http://forums.netbeans.org/)

If you are continuing to have troubles feel free to search through the very helpful Stack Overflow website, at http://stackoverflow.com

## Required Software
- Java SE Development Kit (JDK)
- A C++ compiler; I prefer Minimalist GNU for Windows (http://www.mingw.org/); latest version: http://sourceforge.net/projects/mingw/files/latest/download?source=files

### Installing the Java SE Development Kit (JDK)
- Download the JDK at http://www.oracle.com/technetwork/java/javase/downloads/index.html, by clicking on the Download image underneath "JDK".
- Run the JDK installer and follow the steps.

### Installing MinGW
- Download the MinGW setup executable.
- Run the MinGW setup executable.
- Click the Install button. Click the Continue button.
- Download/Install the necessary components. (Be sure to include the MSYS components.)
- After you have installed the necessary components you will need to configure your Windows PATH variable to include the MinGW utilities; for your benefit here are some video instructions: http://www.youtube.com/watch?v=OHzfmHx2zI4

--- You should now be ready to install NetBeans IDE with little (if none) issues.

## Download and Install NetBeans IDE 7.4
- Browse to https://netbeans.org/downloads/ and click on the Download image below the All column to download NetBeans.
- Run the NetBeans IDE setup executable and follow the instructions.
- DONE!!

#### If you are having further issues, please refer to the NetBeans forums or search through the StackOverflow website for solutions to your specific problems.